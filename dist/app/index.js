var app = angular.module("APP",[]);
app.controller("MainCtrl", function ($scope, $http,$location) {
    var url_service = "http://portal.kimpai.com/portal/tools/leavereport/pulldata.ashx";
    var monthNamesThai = ["มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"];
    $scope.Selmonth = new Date().getMonth();
    $scope.Selyear = new Date().getFullYear();
    $scope.years_data = [];
    $scope.history = false;
    let max_his = 2;
    // given URL http://192.168.55.90:1412/tools/cpm/leave/index.html#?history
    var searchObject = $location.search();
    if (searchObject.history)
    {
        $scope.history = true;
        max_his = 10;
    }
    for(i=0; i<max_his; i++)
    {
        $scope.years_data.push( $scope.Selyear - i )
    }
    LoadData();
    $scope.EventSelMonth = function(){
        LoadData();;
    }
    $scope.EventCLickViewDetail = function(year, emp_id, l_type_grp){
        if ($scope.history){
            $('#modalView').modal('show');
            viewsDetail(year, emp_id, l_type_grp);
        }
    }

    function LoadData()
    {
        $.get("./dist/sql/leave.txt", function(content) {
            let s_year =   $scope.Selyear;
            let s_month = $scope.Selmonth;
            $scope.d_lastday = GetLastDayMonth( parseInt(s_year),parseInt(s_month));
            //console.log(getFormattedDate(d_lastday));
            content = replaceSQL(content,s_year,getFormattedDate($scope.d_lastday));
           
            $http({
                url: url_service,
                method: "POST",
                dataType: 'json',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                data: $.param({
                    db_name : DB_NAME,
                    fn : "query",
                    sql : content
                })
            })
            .then(function (res) {
                $scope.leave_data = res.data.result;
                $scope.leave_data.forEach((el,inx) => {
                    el.SUM_HLN = $scope.ConvertToDateWork(el.SUM_HLN);
                    el.USED_HLN = $scope.ConvertToDateWork(el.USED_HLN);
                    el.USED_SLN = $scope.ConvertToDateWork(el.USED_SLN);
                    el.USED_SLN_NO_MED = $scope.ConvertToDateWork(el.USED_SLN_NO_MED);
                    el.USED_BLP = $scope.ConvertToDateWork(el.USED_BLP);
                    el.USED_BLN = $scope.ConvertToDateWork(el.USED_BLN);
                    el.SUM_BLS = $scope.ConvertToDateWork(el.SUM_BLS);
                    el.USED_BLS = $scope.ConvertToDateWork(el.USED_BLS);
                });
                //console.table($scope.leave_data);
            });
        });
    }



    function getFormattedDate(date) {
        var year = date.getFullYear();
        var month = (1 + date.getMonth()).toString();
        month = month.length > 1 ? month : '0' + month;
        var day = date.getDate().toString();
        day = day.length > 1 ? day : '0' + day;
        return day + '/' + month + '/' + year;
    }
    

    function viewsDetail(year, emp_id, l_type_grp)
    {
       
        $.get("./dist/sql/leave_detail.txt", async function(content) {
            let doc_flag = '%';
            if (l_type_grp == "SLNNM")
            {
                doc_flag = 'F'
                l_type_grp = 'SLN'
            }
            else if (l_type_grp == "SLN")
            {
                doc_flag = 'T'
            }

            content = await replaceSQLDtail(content,year,emp_id,l_type_grp,doc_flag,getFormattedDate($scope.d_lastday));
            console.log(content);
            $http({
                url: url_service,
                method: "POST",
                dataType: 'json',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                data: $.param({
                    db_name : DB_NAME,
                    fn : "query",
                    sql : content
                })
            })
            .then(async function (res) {
                $scope.leave_data_detail = res.data.result;
               //console.table($scope.leave_data_detail);
                });
                
            });
    }

    $scope.getThaiDate =  function getThaiDate(date)
    {
        var year = date.getFullYear() + 543;
        var month = date.getMonth();
        var day = date.getDate().toString();
        day = day.length > 1 ? day : '0' + day;
        return day + ' ' + monthNamesThai[month] + ' ' + year;
    }

    $scope.getPooSoo =  function getPooSoo(s_year)
    {
        return parseInt(s_year) + 543
    }
    function GetLastDayMonth(as_year,as_month)
    {
        var lastDay = new Date(as_year, as_month + 1, 0);
        return lastDay;
    }
    function replaceSQL(content , yyyy , ddmmyyyy )
    {
        content = content.replace( /:as_yyyy/g , yyyy);
        content = content.replace(/:as_ddmmyyyy/g , "'"+ddmmyyyy+"'");
        return content;
    }
    function replaceSQLDtail(content , yyyy , emp_id, l_type_grp , doc_flag , ddmmyyyy )
    {
        content = content.replace( /:as_yyyy/g , "'"+yyyy+"'");
        content = content.replace( /:as_emp_id/g , "'"+emp_id+"'");
        content = content.replace( /:as_l_type_grp/g , "'"+l_type_grp+"'");
        content = content.replace( /:as_doc_flag/g , "'"+doc_flag+"'");
        content = content.replace( /:as_ddmmyyyy/g , "'"+ddmmyyyy+"'");
        return content;
    }
    $scope.ConvertToDateWork =  function ConvertToDateWork(value)
    {
        let day = parseInt(value)
        let hour = parseInt((value - day)*8)
        let minute = parseInt((((value - day)*8) - hour)*60)
        return ((day > 0 ? day + " วัน" : "") + " " +
        (hour > 0 ? hour + " ชั่วโมง" : "") + " " +
        (minute > 0 ? minute + " นาที" : ""));
    }

});