var app = angular.module("APP",[])
{
    //*! บางอย่างมันก็ไม่ออกมา
    // window.onerror = function (msg, url, lineNo, columnNo, error) {
    //     var string = msg.toLowerCase();
    //     var substring = "script error";
    //     if (string.indexOf(substring) > -1){
    //       alert('Script Error: See Browser Console for Detail');
    //     } else {
    //       var message = [
    //         'Message: ' + msg,
    //         'URL: ' + url,
    //         'Line: ' + lineNo,
    //         'Column: ' + columnNo,
    //         'Error object: ' + JSON.stringify(error)
    //       ].join(' - ');
      
    //       alert(message);
    //     }
    //     return false;
    //   };
};

app.directive('modalDetailType', function() {
    return {
        restrict: 'E',
        templateUrl: 'modalDetailType.html'
    };
  });

  app.directive('modalDetailDay', function() {
    return {
        restrict: 'E',
        templateUrl: 'modalDetailDay.html'
    };
  });

app.controller("MainCtrl", function ($scope, $http,$location) {

    $scope.apiMongo = 'http://localhost/portal/mongos.ashx';
    function proxy(context, method, message) { 
        return function() {
            if (message == "Error:")
            {
                let ApiUrl = `${this.apiMongo}?fn=insert`;
                let DATA = 
                {
                    db: 'portal_dev', 
                    coll: "msm_logs",
                    data: {
                        message : arguments,
                        url : window.location.href
                    }
                }
                console.log("AAAAAAAAAAAAAAAAAAAA",arguments);
                // $http.post(ApiUrl, 
                // {
                //     db: "portal_dev", 
                //     coll: "msm_logs",
                //     data: DATA
                // }).then(function (res) {
                //       console.log("Insert Log Success.");
                // }); 
            }
            method.apply(context, [message].concat(Array.prototype.slice.apply(arguments)))
            }
    }
    // let's do the actual proxying over originals
    console.log = proxy(console, console.log, 'Log:')
    console.error = proxy(console, console.error, 'Error:')
    console.warn = proxy(console, console.warn, 'Warning:')





   console.log(user_login, user_name);
   var url_service = "https://portal.kimpai.com/portal/tools/leavereport/pulldata.ashx";
   $scope.loading = true;
   LoadCalendar(new Date().getFullYear())
   var map = {17: false, 77: false, 67: false};
   document.addEventListener('keyup', logKey);
   function logKey(e) {
        if (e.keyCode in map) {
            map[e.keyCode] = true;
            if (map[17] && map[77] && map[67]) {
                localStorage.clear();
                // $('#exampleModalCenter').modal('show');
                console.log("clear");
                map = {17: false, 77: false, 67: false};
            }
        }
        else
        {
            map = {17: false, 77: false, 67: false};
        }
   }
    
    var prople_list = []
    var lis_event1 = [],lis_event2 = [],lis_event3 = [];;
    var lis_event_init = [];
    $scope.toggle_mode_full = localStorage.getItem( "MODE_TOOGLE");
    $scope.toggle_mode_full = $scope.toggle_mode_full == undefined || $scope.toggle_mode_full == 'true' ? true : false
    if ($scope.toggle_mode_full)
        $('#toggle-event').bootstrapToggle('on')
    else
        $('#toggle-event').bootstrapToggle('off')
    $scope.includeDesktopTemplate = false;
    $scope.includeMobileTemplate = false; 
    function reportWindowSize() 
    {
        var screenWidth = window.innerWidth;
        $scope.$apply(()=>
        {
            if (screenWidth < 800)
            {
                $scope.includeMobileTemplate = true;
                $scope.includeDesktopTemplate = false;
            }
            else
            {
                $scope.includeDesktopTemplate = true;
                $scope.includeMobileTemplate = false;
            }
        });
    }
    window.addEventListener('resize', reportWindowSize);
    reportWindowSize();

    async function LoadCalendar(year)
    {
       
        var load_nonwork = ()=>{
            return new Promise((r,j)=>
            {
                $.get("./dist/sql/nonwork.txt", function(content) {
                    content = replaceSQL(content,year);
                    console.log("nonwork sql",content)
                    $http({
                        url: url_service,
                        method: "POST",
                        dataType: 'json',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        },
                        data: $.param({
                            db_name : DB_NAME,
                            fn : "query",
                            sql : content
                        })
                    })
                    .then(function (res) {
                        console.log("nonwork",res)
                        res.data.result.forEach((el,inx) => {
                            var start_date_time =  newUYDate(el.NON_WORK_DATE);
                            var end_date_time = newUYDate(el.NON_WORK_DATE);
                            var startdate = getFormattedDate(start_date_time)
                            var enddate = getFormattedDate(end_date_time)
                            var starttime = "00:00"
                            var endtime = "23:59"
                            let color = "#FF7F27";
                            if (el.NON_WORK_DESC != null)
                            {
                                lis_event_init.push({
                                    "id": lis_event_init.length+1,
                                    "name": "วันหยุด<br />"+el.NON_WORK_DESC,
                                    "startdate": startdate,
                                    "enddate": enddate,
                                    "starttime": starttime,
                                    "endtime": endtime,
                                    "color":color,
                                    "url": ""
                                })
                            };
                        });
                    })
                    .then(()=>r(1));
                });
            })
        };
        await load_nonwork();
        var load_sut_sun = ()=>
        {
                var start_date = new Date(year,1,1);
                console.log(start_date);
                var day = 60 * 60 * 24 * 1000;
                for(var i=1;i<366;i++)
                {
                    var curr = new Date(start_date.getTime() + day*i);
                    if (curr.getDay() == 0 || curr.getDay() == 6 )
                    {
                        let color = "#E1A57C";
                        var startdate = getFormattedDate(curr)
                        var enddate = getFormattedDate(curr)
                        var starttime = "00:00"
                        var endtime = "23:59"
                        lis_event_init.push({
                            "id": lis_event_init.length+1,
                            "name": "วันหยุด<br />สุดสัปดาห์",
                            "startdate": startdate,
                            "enddate": enddate,
                            "starttime": starttime,
                            "endtime": endtime,
                            "color":color,
                            "url": ""
                        })
                    }
                }
        }
        load_sut_sun();
        $.get("./dist/sql/leave_detail.txt", function(content) {
            content = replaceSQLDtail(content,year,"%","%","%","31/12/"+year);
            console.log(content);
            $http({
                url: url_service,
                method: "POST",
                dataType: 'json',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                data: $.param({
                    db_name : DB_NAME,
                    fn : "query",
                    sql : content
                })
            })
            .then(function (res) {
                leave_detail_res = res;
                SetCalendar(res) 
                });

                
            });
    }

    var leave_detail_res;
    $scope.mode = "NORMAL";
    function SetCalendar(res)
    {
        var url_string = window.location.href;
        var url = new URL(url_string);
        var mode = url.searchParams.get("mode") == undefined ? 'NORMAL' : url.searchParams.get("mode");
        console.log("mode" , mode );
        if ( mode.toUpperCase() == "FAC")
        {
            $scope.mode = "FACTORY";
            lis_event3 = [...lis_event_init];
            var gen_event3 =  new Promise((resolve, reject) => 
            {
                //แบบย่อ
                    var date_type_leave = res.data.result.reduce((r,a)=>
                    {
                        var key = a.LEAVE_DATE
                        r[key] = [...r[key] || [], a];
                        return r;
                    }, {});
                    console.log("date_type_leave", date_type_leave);
                    var keyobject =  Object.keys(date_type_leave)
                    if (keyobject.length == 0) resolve(lis_event3);
                    keyobject.forEach((el,inx)=>
                        {
                            var leave_date = el;
                            var start_date_time =  new Date(leave_date);
                            var end_date_time = new Date(leave_date);
                            var startdate = getFormattedDate(start_date_time);
                            var enddate = getFormattedDate(end_date_time);
                            var starttime = getFormattedTime(start_date_time);
                            var endtime = getFormattedTime(end_date_time);
                            find_color = localStorage.getItem( "FAC-"+leave_date );
                            if (find_color != undefined)
                                var color = find_color
                            else
                            {
                                var color = getRandomColor()
                                localStorage.setItem(  "FAC-"+leave_date, color);
                            }
                            var id = lis_event3.length+1;
                            lis_event3.unshift({
                                "id": id,
                                "name": `${date_type_leave[el].length} คน`,
                                "startdate": startdate,
                                "enddate": enddate,
                                "starttime": starttime,
                                "endtime": endtime,
                                "color":color,
                                "data": date_type_leave[el]
                            });
                            if (inx === keyobject.length -1) resolve(lis_event3);
                        });
                }); 
                gen_event3.then((lis_event3)=>
                {
                    console.log("lis_event3",lis_event3);
                    $('#mycalendar3').monthly({
                        mode: 'event',
                        dataType: 'json',
                        events: {
                            "monthly": lis_event3 
                        }
                    });
                });
        }
        else
        {
            $scope.mode = "NORMAL";
            lis_event1 = [...lis_event_init];
            lis_event2 = [...lis_event_init];
            var gen_event2 =  new Promise((resolve, reject) => 
            {
                //แบบย่อ
                    var date_type_leave = res.data.result.reduce((r,a)=>
                    {
                        var key = a.LEAVE_DATE+'$$$'+a.L_TYPE_GRP_DESC
                        r[key] = [...r[key] || [], a];
                        return r;
                    }, {});
                    console.log(date_type_leave);
                    var keyobject =  Object.keys(date_type_leave)
                    if (keyobject.length == 0) resolve(lis_event2);
                    keyobject.forEach((el,inx)=>
                        {
                            var split_str = el.split("$$$");
                            var leave_date = split_str[0];
                            var type_desc = split_str[1];
                            var start_date_time =  new Date(leave_date);
                            var end_date_time = new Date(leave_date);
                            var startdate = getFormattedDate(start_date_time);
                            var enddate = getFormattedDate(end_date_time);
                            var starttime = getFormattedTime(start_date_time);
                            var endtime = getFormattedTime(end_date_time);
                            find_color = localStorage.getItem( type_desc );
                            if (find_color != undefined)
                                var color = find_color
                            else
                            {
                                var color = getRandomColor()
                                localStorage.setItem( type_desc, color);
                            }
                            var id = lis_event2.length+1;
                            lis_event2.push({
                                "id": id,
                                "name": `(${date_type_leave[el].length} คน) ${type_desc}`,
                                "startdate": startdate,
                                "enddate": enddate,
                                "starttime": starttime,
                                "endtime": endtime,
                                "color":color,
                                "data": date_type_leave[el]
                            });
                            if (inx === keyobject.length -1) resolve(lis_event2);
                        });
                }); 
                var gen_event1 =  new Promise((resolve, reject) => 
                {
                    //แบบเต็ม
                    if (res.data.result.length == 0) resolve(lis_event1);
                    res.data.result.forEach((el,inx) => {
                        var start_date_time =  newUYDate(el.START_TIME);
                        var end_date_time = newUYDate(el.END_TIME);
                        var startdate = getFormattedDate(start_date_time)
                        var enddate = getFormattedDate(end_date_time)
                        var starttime = getFormattedTime(start_date_time)
                        var endtime = getFormattedTime(end_date_time)
                        find_color = localStorage.getItem( el.EMP_NAME );
                        if (find_color != undefined)
                            var color = find_color
                        else
                        {
                            var color = getRandomColor()
                            localStorage.setItem( el.EMP_NAME, color);
                        }
                        var find_color = prople_list.find((r)=>r.id == el.EMP_ID)
                        if (find_color == undefined)
                        {
                            prople_list.push({
                                id : el.EMP_ID,
                                name : el.EMP_NAME,
                                color : color
                            })
                        }
                        var appv = ""
                        if (el.MGR_APPV_FLAG == "P")
                            appv = "(**รอหัวหน้าอนุมัติ**)<br /> "
                        else if (el.MGR_APPV_FLAG == "A" && el.HR_APPV_FLAG == "P")
                            appv = "(**รอ HR อนุมัติ**)<br /> "
                        lis_event1.push({
                            "id": lis_event1.length+1,
                            "name": appv+el.L_TYPE_GRP_DESC +" "+ starttime + "-" +  endtime + "<br /> " + el.EMP_NAME,
                            "startdate": startdate,
                            "enddate": enddate,
                            "starttime": starttime,
                            "endtime": endtime,
                            "color":color,
                            "url": ""
                        })
                        if (inx === res.data.result.length -1) 
                            resolve(lis_event1)
                    });
                });
                gen_event1.then((lis_event1)=>
                {
                    console.log("lis_event", lis_event1);
                    $('#mycalendar1').monthly({
                        mode: 'event',
                        dataType: 'json',
                        events: {
                            "monthly": lis_event1 
                        }
                    });
                });
                gen_event2.then((lis_event2)=>
                {
                    console.log("lis_event2",lis_event2);
                    $('#mycalendar2').monthly({
                        mode: 'event',
                        dataType: 'json',
                        events: {
                            "monthly": lis_event2 
                        }
                    });
                });
        }

        $scope.loading = false;

      
    }

    //Add Event 
    //เวลาคลิกที่ข้อมูล ให้ Pop รายละเอียดมา
    $scope.modalData = [];
    $(document.body).on("click touchstart", "#mycalendar2 .monthly-event-indicator", function (event) {
        var id = $(this).data("eventid");
        $scope.$apply(()=>
        {
            $scope.modalData = lis_event2[id-1];
            console.log(id , $scope.modalData)
            if ( $scope.modalData.data != undefined)
            {
                $("#ModalType").modal('show')
            }
        });
       
    });
    $(document.body).on("click touchstart", "#mycalendar3 .monthly-event-indicator", function (event) {
        var id = $(this).data("eventid");
        $scope.$apply(()=>
        {
            $scope.list_type_grp_filter = "";
            var find = lis_event3.find(r=>r.id == id)
            $scope.modalData = find != undefined ? find : undefined;
            console.log("Click :" , id , $scope.modalData , $scope.key_dropdown_type_grp)
            if ( $scope.modalData.data != undefined)
            {
                $("#ModalDay").modal('show')
            }
            var dropdown_type_grp = $scope.modalData.data.reduce((r,a)=>
            {
                var key = a.L_TYPE_GRP_DESC
                r[key] = [...r[key] || [], a];
                return r;
            }, {});
            $scope.key_dropdown_type_grp =  Object.keys(dropdown_type_grp)
        });
       
    });

    //เปลี่ยน Toogle
    $('#toggle-event').change(function() {
        $scope.$apply(()=>
        {
            $scope.toggle_mode_full = !$scope.toggle_mode_full;
            localStorage.setItem( "MODE_TOOGLE" , $scope.toggle_mode_full  );
        });
      });
      $('#listTypeGrp').change(function() {
        $scope.$apply(()=>
        {
          var data =  $scope.modalData.data;

        });
      });



    function replaceSQL(content , yyyy  )
    {
        content = content.replace( /:as_yyyy/g , yyyy);
        return content;
    }
    function replaceSQLDtail(content , yyyy , emp_id, l_type_grp , doc_flag , ddmmyyyy )
    {
        content = content.replace( /:as_yyyy/g , "'"+yyyy+"'");
        content = content.replace( /:as_emp_id/g , "'"+emp_id+"'");
        content = content.replace( /:as_holder_id/g , "'"+user_login+"'");
        content = content.replace( /:as_l_type_grp/g , "'"+l_type_grp+"'");
        content = content.replace( /:as_doc_flag/g , "'"+doc_flag+"'");
        content = content.replace( /:as_ddmmyyyy/g , "'"+ddmmyyyy+"'");
        return content;
    }

    function getFormattedDate(date) {
        var year = date.getFullYear();
        var month = (1 + date.getMonth()).toString();
        month = month.length > 1 ? month : '0' + month;
        var day = date.getDate().toString();
        day = day.length > 1 ? day : '0' + day;
        return year + '-' + month + '-' + day;
    }
    function getFormattedTime(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        return hours+":"+minutes;
    }
   
    function newUYDate(pDate) {
        let dd = pDate.split("/")[0].padStart(2, "0");
        let mm = pDate.split("/")[1].padStart(2, "0");
        let yyyy = pDate.split("/")[2].split(" ")[0];
        let hh = pDate.split("/")[2].split(" ")[1].split(":")[0].padStart(2, "0");
        let mi = pDate.split("/")[2].split(" ")[1].split(":")[1].padStart(2, "0");
      
        mm = (parseInt(mm) - 1).toString(); // January is 0
      
        return new Date(yyyy, mm, dd, hh, mi);
      }

      function getRandomColor() {
        // var letters = '0123456789ABCDEF';
        // var color = '#';
        // for (var i = 0; i < 6; i++) {
        //   color += letters[Math.floor(Math.random() * 16)];
        // }
        // return color;
        var o = Math.round, r = Math.random, s = 255;
        return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + 0.6 + ')';
      }

      $scope.dateformat = (date_string) =>
      {
            var arr = date_string.split('-');
            return arr[2] + '/' + arr[1] + '/' + arr[0];

      }

});

