﻿<%@ WebHandler Language="C#" Class="pulldata" %>

using System;
using System.Web;

public class pulldata : IHttpHandler {

    
    public void ProcessRequest (HttpContext context) {
         try
        {
            context.Response.ContentType = "text/plain";
            string fn = context.Request.Form["fn"];
            object wrapper = null;
            switch (fn.ToLower())
            { 
                case "query":
                    wrapper = Query(context);
                    break;
            }
            context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(wrapper));
        }
         catch (Exception ex)
         {
             var wrapper = new { result = ex.Message };
             context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(wrapper));
         }
    }

    private object Query(HttpContext context)
    {
        try
        {
            string sql = context.Request.Form["sql"];
            string db_name = context.Request.Form["db_name"];
            Oracle o = new Oracle(db_name,"KPRUSR","kprusr");
            System.Data.DataTable dt = o.Execute(sql);
            return new { result = dt, code = 200 };
        }
        catch (Exception ex)
        {
            return new { result = ex.Message, code = 500 };
        }
        
    }

    
    public bool IsReusable {
        get {
            return false;
        }
    }

}